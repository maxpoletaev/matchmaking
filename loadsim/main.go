package main

import (
	"flag"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strings"
	"sync"
	"time"
)

type options struct {
	workers int
	count   int
	rps     int
}

func simulate(ch <-chan struct{}) {
	client := http.Client{}

	for range ch {
		var (
			name    = fmt.Sprintf("P%06d", rand.Intn(100000))
			skill   = rand.Float64() * 1000
			latency = rand.Float64() * 300
		)

		body := fmt.Sprintf(`{"name": "%s", "latency": %f, "skill": %f}`, name, latency, skill)
		bodyReader := strings.NewReader(body)

		req, _ := http.NewRequest("POST", "http://localhost:8080/users", bodyReader)
		req.Header.Set("Content-Type", "application/json")

		resp, err := client.Do(req)
		if err != nil {
			log.Printf("error: %s", err)
			return
		}

		_ = resp.Body.Close()
	}
}

func main() {
	var opts options
	flag.IntVar(&opts.workers, "workers", 1, "Number of workers")
	flag.IntVar(&opts.count, "count", 100, "Number of requests")
	flag.IntVar(&opts.rps, "rps", 10, "Requests per second")
	flag.Parse()

	log.Printf("workers: %d, requests: %d", opts.workers, opts.count)
	ch := make(chan struct{})
	wg := sync.WaitGroup{}
	wg.Add(opts.workers)

	for i := 0; i < opts.workers; i++ {
		go func() {
			simulate(ch)
			wg.Done()
		}()
	}

	ticker := time.NewTicker(time.Second / time.Duration(opts.rps))
	defer ticker.Stop()

	for i := 0; i < opts.count; i++ {
		<-ticker.C
		ch <- struct{}{}

		if i != 0 && i%1000 == 0 {
			log.Printf("sent %d requests", i)
		}
	}

	close(ch)
	wg.Wait()
}
