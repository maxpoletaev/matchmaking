package main

import (
	"context"
	"errors"
	"flag"
	"log"
	"net/http"
	"os"
	"os/signal"
	"runtime/pprof"
	"sync"
	"syscall"
	"time"
)

type options struct {
	Addr        string
	RunInterval time.Duration
	CPUProfile  string
	GroupSize   int
}

type runGroup struct {
	wg sync.WaitGroup
}

func (rg *runGroup) Go(f func()) {
	rg.wg.Add(1)
	go func() {
		defer rg.wg.Done()
		f()
	}()

}

func (rg *runGroup) Wait() {
	rg.wg.Wait()
}

func main() {
	var opts options
	flag.StringVar(&opts.Addr, "addr", ":8080", "listen address")
	flag.DurationVar(&opts.RunInterval, "interval", 1*time.Second, "matchmaking run interval")
	flag.StringVar(&opts.CPUProfile, "cpuprof", "", "write cpu profile to file")
	flag.IntVar(&opts.GroupSize, "groupsize", 8, "group size")
	flag.Parse()

	if opts.CPUProfile != "" {
		f, err := os.Create(opts.CPUProfile)
		if err != nil {
			log.Fatalf("could not create CPU profile: %s", err)
		}

		defer func() {
			_ = f.Close()
		}()

		if err := pprof.StartCPUProfile(f); err != nil {
			log.Fatalf("could not start CPU profile: %s", err)
		}

		defer pprof.StopCPUProfile()
	}

	log.SetOutput(os.Stderr)
	log.Printf("[INFO] group size: %d, matchmaking interval: %s", opts.GroupSize, opts.RunInterval)

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

	matchmaker := NewKMeansMatchmaker(KMeansMatchmakerConfig{
		RunInterval: opts.RunInterval,
		GroupSize:   opts.GroupSize,
		Tolerance:   0.001,
		MaxIter:     100,
		KClusters:   100,
	})

	appCtx, cancel := context.WithCancel(context.Background())
	service := NewService(matchmaker)
	rg := runGroup{}

	rg.Go(func() {
		if err := service.Run(appCtx, opts.Addr); err != nil {
			if !errors.Is(err, http.ErrServerClosed) {
				log.Printf("[ERROR] HTTP server: %s", err)
			}
		}
	})

	<-signalChan
	log.Printf("[INFO] shutting down...")
	cancel()
	matchmaker.Stop()
	rg.Wait()
}
