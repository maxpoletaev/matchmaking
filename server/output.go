package main

import (
	"fmt"
	"math"
	"slices"
)

func mean(data []float64) float64 {
	var sum float64
	for _, x := range data {
		sum += x
	}

	return sum / float64(len(data))
}

func stdDev(data []float64) float64 {
	m := mean(data)

	var variance float64
	for _, x := range data {
		variance += fastSquare(x - m)
	}

	variance /= float64(len(data))

	return math.Sqrt(variance)
}

func printGroups(groups []Group) {
	if len(groups) == 0 {
		return
	}

	var (
		names     []string
		skills    []float64
		latencies []float64
		durations []float64
	)

	for _, group := range groups {
		names = names[:0]
		skills = skills[:0]
		latencies = latencies[:0]
		durations = durations[:0]

		for _, p := range group.Players {
			names = append(names, p.Name)
			skills = append(skills, p.Skill)
			latencies = append(latencies, p.Latency)

			timeSpent := group.Timestamp - p.Timestamp
			durations = append(durations, float64(timeSpent))
		}

		avgLatency, minLatency, maxLatency := mean(latencies), slices.Min(latencies), slices.Max(latencies)
		avgTime, minTime, maxTime := mean(durations), slices.Min(durations), slices.Max(durations)
		avgSkill, minSkill, maxSkill := mean(skills), slices.Min(skills), slices.Max(skills)
		stdDevLatency, stdDevSkill := stdDev(latencies), stdDev(skills)

		fmt.Printf("Group %d:\n", group.GroupID)
		fmt.Printf("  Latency Min/Max/Avg: %.2f / %.2f / %.2f\n", minLatency, maxLatency, avgLatency)
		fmt.Printf("  Skill Min/Max/Avg: %.2f / %.2f / %.2f\n", minSkill, maxSkill, avgSkill)
		fmt.Printf("  Time Min/Max/Avg: %.2f / %.2f / %.2f\n", minTime, maxTime, avgTime)
		fmt.Printf("  StdDev Latency/Skill: %.2f / %.2f\n", stdDevLatency, stdDevSkill)
		fmt.Printf("  Players: %s\n", names)

		//for _, p := range group.Players {
		//	timeSpent := group.Timestamp - p.Timestamp
		//	fmt.Printf("    %s: Latency:%.2f, Skill:%.2f, Time:%dms \n", p.Name, p.Latency, p.Skill, timeSpent)
		//}

		fmt.Println()
	}
}
