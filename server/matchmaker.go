package main

import (
	"cmp"
	"log"
	"math/rand"
	"slices"
	"sync"
	"time"
)

const (
	maxBufferSize = 100000
)

type Player struct {
	Name        string  `json:"name"`
	Skill       float64 `json:"skill"`
	Latency     float64 `json:"latency"`
	LatencyNorm float64 `json:"-"`
	SkillNorm   float64 `json:"-"`
	Timestamp   int64   `json:"-"`
}

type Cluster struct {
	Players []Player
	Center  Player
	Sorted  bool
}

func fastSquare(x float64) float64 {
	return x * x // much faster than math.Pow(x, 2)
}

func squaredEuclideanDistance(p1, p2 *Player) float64 {
	return fastSquare(p1.LatencyNorm-p2.LatencyNorm) + fastSquare(p1.SkillNorm-p2.SkillNorm)
}

func findClosestCluster(player *Player, clusters []Cluster) int {
	minDistance := squaredEuclideanDistance(player, &clusters[0].Center)
	closest := 0

	for j := 1; j < len(clusters); j++ {
		distance := squaredEuclideanDistance(player, &clusters[j].Center)
		if distance < minDistance {
			minDistance = distance
			closest = j
		}
	}

	return closest
}

func assignPlayersToClusters(players []Player, clusters []Cluster) {
	for i := range clusters {
		clusters[i].Players = clusters[i].Players[:0]
	}

	// TODO: probably can be parallelized
	for i := range players {
		player := &players[i]
		clusterIdx := findClosestCluster(player, clusters)
		clusters[clusterIdx].Players = append(clusters[clusterIdx].Players, *player)
	}
}

func updateClusterCentroids(clusters []Cluster, tolerance float64) bool {
	moved := false

	wg := sync.WaitGroup{}
	wg.Add(len(clusters))

	for i, cluster := range clusters {
		var latencySum, skillSum float64
		for _, p := range cluster.Players {
			latencySum += p.LatencyNorm
			skillSum += p.SkillNorm
		}

		if len(cluster.Players) > 0 {
			newCenter := Player{
				LatencyNorm: latencySum / float64(len(cluster.Players)),
				SkillNorm:   skillSum / float64(len(cluster.Players)),
			}

			if squaredEuclideanDistance(&newCenter, &cluster.Center) > tolerance {
				clusters[i].Center = newCenter
				moved = true
			}
		}
	}

	return moved
}

func kMeans(clusters []Cluster, players []Player, tolerance float64, maxIter int) {
	for i := range clusters {
		// Take a random player as the initial center. Can be improved with KMeans++
		clusters[i].Center = players[rand.Intn(len(players))]
	}

	for i := 0; i < maxIter; i++ {
		assignPlayersToClusters(players, clusters)

		if !updateClusterCentroids(clusters, tolerance) {
			break
		}
	}
}

type Group struct {
	GroupID   int
	Timestamp int64
	Players   []Player
}

type KMeansMatchmakerConfig struct {
	RunInterval time.Duration
	GroupSize   int
	Tolerance   float64
	MaxIter     int
	KClusters   int
}

type KMeansMatchmaker struct {
	playerBuffer1 []Player
	playerBuffer2 []Player
	playerPool    []Player
	mut           sync.Mutex
	lastGroupID   int

	runInterval time.Duration
	groupSize   int
	tolerance   float64
	maxIter     int
	kClusters   int

	shutdown chan struct{}
	stopped  chan struct{}
}

func NewKMeansMatchmaker(params KMeansMatchmakerConfig) *KMeansMatchmaker {
	m := &KMeansMatchmaker{
		runInterval: params.RunInterval,
		groupSize:   params.GroupSize,
		tolerance:   params.Tolerance,
		maxIter:     params.MaxIter,
		kClusters:   params.KClusters,
		shutdown:    make(chan struct{}),
		stopped:     make(chan struct{}),
	}

	go func() {
		m.start()
		close(m.stopped)
	}()

	return m
}

func (m *KMeansMatchmaker) AddPlayer(p Player) bool {
	m.mut.Lock()
	defer m.mut.Unlock()

	// Limit the buffer size to provide some backpressure if we’re not keeping up.
	if len(m.playerBuffer1) >= maxBufferSize {
		return false
	}

	// The values need to be scaled to be in the same range.
	p.LatencyNorm = p.Latency / 300.0
	p.SkillNorm = p.Skill / 1000.0

	m.playerBuffer1 = append(m.playerBuffer1, p)
	return true
}

func (m *KMeansMatchmaker) start() {
	ticker := time.NewTicker(m.runInterval)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			matches := m.matchPlayers()
			printGroups(matches)
		case <-m.shutdown:
			return
		}
	}
}

func (m *KMeansMatchmaker) Stop() {
	close(m.shutdown)
	<-m.stopped
}

func (m *KMeansMatchmaker) matchPlayers() (groups []Group) {
	m.mut.Lock()
	// Swap the buffers to avoid locking for too long.
	m.playerBuffer1, m.playerBuffer2 = m.playerBuffer2, m.playerBuffer1
	m.mut.Unlock()

	// Copy the player buffer to the player pool and clear the buffer.
	m.playerPool = append(m.playerPool, m.playerBuffer2...)
	m.playerBuffer2 = m.playerBuffer2[:0]

	// Not enough players to form a group.
	if len(m.playerPool) < m.groupSize {
		return nil
	}

	log.Printf("[INFO] running matchmaker: %d players in the pool", len(m.playerPool))
	numClusters := m.kClusters
	groupSize := m.groupSize
	startTime := time.Now()

	for len(m.playerPool) >= groupSize {
		// Run the K-means algorithm to group players into clusters.
		clusters := make([]Cluster, numClusters)
		kMeans(clusters, m.playerPool, m.tolerance, m.maxIter)

		for i := range clusters {
			// Prioritize players that have been in the pool for longer.
			// We will be removing them from the end, so it should be in reverse order.
			slices.SortFunc(clusters[i].Players, func(l, r Player) int {
				return cmp.Compare(r.Timestamp, l.Timestamp)
			})

			// Divide the cluster into groups of groupSize.
			for len(clusters[i].Players) >= groupSize {
				offset := len(clusters[i].Players) - groupSize
				groupPlayers := clusters[i].Players[offset:]
				clusters[i].Players = clusters[i].Players[:offset]

				groups = append(groups, Group{
					Timestamp: time.Now().UnixMilli(),
					GroupID:   m.lastGroupID,
					Players:   groupPlayers,
				})

				m.lastGroupID++
			}
		}

		// Collect the remaining players.
		m.playerPool = m.playerPool[:0]
		for _, cluster := range clusters {
			m.playerPool = append(m.playerPool, cluster.Players...)
		}

		// Will try again with fewer clusters, if there are still unmatched players.
		numClusters = max(numClusters/2, 1)
	}

	elapsed := time.Since(startTime)
	log.Printf("[INFO] elapsed time:%s, %d players left unmatched", elapsed, len(m.playerPool))

	return groups
}
