package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"
)

type matchmaker interface {
	AddPlayer(player Player) bool
}

type Service struct {
	matchmaker matchmaker
	server     *http.Server
}

func NewService(matchmaker matchmaker) *Service {
	return &Service{
		matchmaker: matchmaker,
	}
}

func (s *Service) postUsers(w http.ResponseWriter, r *http.Request) {
	var player Player

	if err := json.NewDecoder(r.Body).Decode(&player); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	player.Timestamp = time.Now().UnixMilli()
	ok := s.matchmaker.AddPlayer(player)

	if !ok {
		http.Error(w, "too many requests", http.StatusTooManyRequests)
		return
	}

	w.WriteHeader(http.StatusCreated)
}

func (s *Service) Run(ctx context.Context, addr string) error {
	mux := http.NewServeMux()
	mux.HandleFunc("/users", s.postUsers)

	s.server = &http.Server{
		Addr:    addr,
		Handler: mux,
	}

	go func() {
		<-ctx.Done()

		if err := s.server.Shutdown(context.Background()); err != nil {
			log.Printf("[ERROR] failed to shutdown http server: %s", err)
		}
	}()

	log.Printf("[INFO] starting HTTP server on %s", addr)

	if err := s.server.ListenAndServe(); err != nil {
		if errors.Is(err, http.ErrServerClosed) {
			return nil
		}

		return fmt.Errorf("listen and serve: %w", err)
	}

	return nil
}
