# Matchmaking Service

## Requirements

 * Go 1.21+
 * No external dependencies, stdlib only

## Building and running

Build things:

```sh
$ make build
```

Start the matchmaking server:

```sh
$ bin/server -groupsize=8 -interval=1s
```

Send 100K matchmaking requests at 10K RPS:

```sh
$ bin/loadsim -workers=10 -count=100000 -rps=10000
```

## Request example

```http request
POST /users
Content-Type: application/json

{
  "name": "P1",
  "skill": 400.0, // 0-1000
  "latency": 100.0 // 0-300
}
````

## Output format

```text
Group 10764:
  Latency Min/Max/Avg: 135.87 / 158.52 / 146.15
  Skill Min/Max/Avg: 860.77 / 930.90 / 892.98
  Time Min/Max/Avg: 730.00 / 822.00 / 771.10
  StdDev Latency/Skill: 6.67 / 24.00
  Players: [P089474 P080764 P090061 P030383 P076437 P056974 P092388 P006863 P012981 P049688]
```

## Matchmaking algorithm

The approach is based on a K-means clustering algorithm [[1]]. It is efficient 
enough for our case and also produces better results (in terms of minimizing the 
standard deviation between the players in the group) than a simpler greedy algorithm. 
It can also be easily extended to support more dimensions in matchmaking parameters 
(e.g., player level, age, toxicity, etc.) if needed.

On each POST `/users` request, the players are continuously added to the in-memory 
player pool. The background process, which runs at every fixed interval, divides 
the player pool into 100 (configurable) clusters by representing the player as a
2D vertex in the Skill-Latency space.

![Clustering Simulation](plot.png)

The resulting clusters will contain an unsorted set of players with similar skill
and latency values. Each cluster is then sorted by the player's timestamp to 
minimize the overall wait time for the players.

If the cluster has enough players to form a group, groupSize players are removed
from the cluster and the pool, and the group details are printed to the console. 
The algorithm runs until there are no more players left in the pool to form a group.

The players that are not matched are kept in the pool for the next run. They will 
be prioritized and matched first in the next iteration.

The base complexity of the K-means is O(NK), where N is the number of players, 
and K is the number of clusters into which the players are divided. This 
implementation uses a constant value for K, so the complexity converges to O(N).
However, an additional sorting step is required to prioritize the players with 
longer wait times, which effectively makes the overall complexity of the 
algorithm is O(N log(N)).

## Load considerations

Let us take Fortnite as a baseline. Based on publicly available data [[2]],
Fortnite has around 3.5M players online on average, with a peak of 11M during
special events.

Let’s also assume that a typical game session lasts for 20 minutes, and the
player spends around 1 minute between the games in the matchmaking queue, lobby,
etc. This means that on average, we should expect around 1/20 of the player base
to be in the matchmaking queue at any given time. This effectively gives us around
175K players entering the matchmaking queue every minute, or around 3K players
every second. Therefore, a good solution should aim to produce at least 3K
high-quality matches per second and also have some headroom for peak times.

## Horizontal scaling

The service is stateful, which means that scaling it horizontally is not trivial.
The state cannot be easily put into external storage, as the matchmaker needs 
all the data to be available locally to perform the clustering efficiently.

One way to scale the service is to use a sharding approach. The player pool can 
be divided into multiple shards, and each shard can be processed by a separate
matchmaking instance.

The shards can be divided based on the player's geographical location (EU, US, 
Asia, etc.) and further subdivided based on the player's skill range if needed 
(we can assume that players from different regions as well as low and high-skilled
players are unlikely to be matched anyway).

To avoid situations when there are too few players in a particular shard the 
user is assigned to, the game client can decide to join multiple shards at once.

## Fault tolerance

To properly handle failure scenarios, the matchmaking service can be split into 
two parts: the stateless frontend and the stateful backend.

**Frontend:** is trivial to scale horizontally and can be used to distribute the
incoming requests to the backend instances (e.g., via RabbitMQ, which allows 
flexible message routing). Frontend instances will also be responsible for 
keeping track of the user matchmaking requests and persisting the ticket 
information in the Redis Cluster (which is, by definition, fault-tolerant and 
can be easily scaled).

**Backend:** will be responsible for the actual matchmaking and will consume the 
requests from the matchmaking queue and perform all the heavy lifting. Once the 
match is found, the backend will save the results to the Redis Cluster by 
updating the ticket object.

**Failover:** Each shard should contain at least two instances of the backend 
service. The backend will be listening to a queue in the "Single Active 
Consumer" mode, so only one instance within a shard will be processing the 
requests at a time. In case of failure, the next instance will take over and 
continue processing the requests. The data stored in the backend instance is not
critical and can be easily restored from the ticket state persisted in Redis or 
by retrying the request from the game client. The backends should also 
periodically report their health status to Redis, for the frontend to understand
that a backend instance owning a particular ticket is still alive and if a retry 
is needed.

![Diagram](diagram.png)

## References

 * \[1] [Scikit-learn: Clustering][1]
 * \[2] [Fortnite Player Count][2]


[1]: https://scikit-learn.org/stable/modules/clustering.html#k-means
[2]: https://fortnite.gg/player-count
